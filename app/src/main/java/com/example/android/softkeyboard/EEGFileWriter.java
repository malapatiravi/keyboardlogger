package com.example.android.softkeyboard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by malz on 11/25/16.
 */
/*
This class provides the interface for writing the data into file after.
This is wrapper for file writer class. this can be used for any requirement.
 */
public class EEGFileWriter {
    String path;
    BufferedWriter dataWriter;
    File dataFile;
    /*
    Calling constructor is must for this class. The file name with full path should be passed
     */
    public EEGFileWriter(String _path)
    {
        path=_path;
        Initialize();
    }
    /*
    Initialize is though defined public it should be used as private
     */
    public void Initialize()
    {
        dataFile=new File(path);
        try
        {
            dataWriter=new BufferedWriter(new FileWriter(dataFile));
            eraseFile();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
    This function is called to erase the already written lines into file
     */
    public void eraseFile()
    {
        try
        {
            dataWriter=new BufferedWriter(new FileWriter(dataFile));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
    This function is used to write a single line at a time to the file.
     */
    public void writeLine(String aLine)
    {
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /*
    This function will add the imposter attribute to the end like wise for Genuine genuine is added at the end
     */

    public void writeLineImposter(String aLine)
    {
        /*
        Adding Imposter to the end of line always
         */
        aLine=aLine+",Imposter";
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
    This function will add the imposter attribute to the end like wise for Genuine genuine is added at the end
     */
    public void writeLineGenuine(String aLine)
    {
        aLine=aLine+",Genuine";
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
     This is called to close the file once everything is written.
     */
    public void closeFile()
    {
        try
        {
            dataWriter.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

     /*
    returns the file name without file name
     */

    public String getPathwithOutFileName(String _path)
    {
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    public String getPathwithOutFileName()
    {
        String _path=path;
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
}
