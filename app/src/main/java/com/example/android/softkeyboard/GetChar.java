package com.example.android.softkeyboard;

import java.util.ArrayList;

/**
 * Created by malz on 12/21/16.
 * character handling for individual characters
 * This can be discarded
 */

public class GetChar {
    ArrayList<String> getChar;
    public GetChar()
    {
        getChar=new ArrayList<String>();
        for(int i=0;i<256;i++)
        {
            if((i>=48 && i<=122)||(i>=97 && i<=122))
            {
                getChar.add(""+(char)i);
            }
            else if(i>122)
                getChar.add(""+(char)i);
        }
        getChar.add(8,"Back Space");
        getChar.add(32,"Space");
        getChar.add(24,"Cancel");
        getChar.add(127,"Delete");
    }
    public String getNow(int ind)
    {
        return getChar.get(ind);
    }
}
