package com.example.android.softkeyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by malz on 11/25/16.
 * This class helps to read the EEGFile
 */
public class EEGFileReader {
    String path;
    BufferedReader dataReader;
    File dataFile;
    public EEGFileReader(String _path)
    {
        path=_path;
        Initialize();
    }
    public void Initialize()
    {
        try
        {
            dataFile=new File(path,"");
            dataReader=new BufferedReader(new FileReader(dataFile));
            //Log.i("File Path is",dataFile.getName()+"/"+dataFile.getPath()+"/"+dataFile.getCanonicalFile());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
    /*
    This fucntion was intended to filter unwanted data but this is handled in another place
    If you are following the code then the pre processing stem includes the deletion of waste data.
     */
    public void removeWasteData()
    {

    }
    /*
     * returns the file name
     */
    public String getFileName()
    {
        return dataFile.getName();
    }
    /*
    Returns the path without the file name
     */
    public String getPathwithOutFileName(String _path)
    {
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    /*
    returns the file name without file name
     */
    public String getPathwithOutFileName()
    {
        String _path=path;
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    /*
     Returns the next line. The object maintains the pointer while accessing the data.
     Partial singleton pattern is implemneted. There can be only one pointer.
     */
    public String getNextLine()
    {
        try
        {
            String aLine;

            if((aLine=dataReader.readLine())!=null)
            {
                return aLine;
            }
            else
                return "null";
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "null";
        }

    }
    /*
    Closes the file. This is must to call after all the data is written.
     */
    public void closeFile()
    {
        try
        {
            dataReader.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
